//
//  ObservationContainerTests.m
//  ObservationContainerTests
//
//  Created by fadi on 6/2/18.
//  Copyright © 2018 fadi. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "Observer.h"

@interface SomeObservableObject: NSObject
@property (assign, nonatomic) NSUInteger number;
@property (copy  , nonatomic) NSString *string;
@end

@interface SomeNestedObservableObject: NSObject
@property (assign, nonatomic) NSUInteger numberInParent;
@property (strong, nonatomic) SomeObservableObject *child;
@end

@implementation SomeObservableObject

@end

@implementation SomeNestedObservableObject

@end

@interface ObservationContainerTests : XCTestCase

@property (strong, nonatomic) SomeNestedObservableObject *obj;

@end

@implementation ObservationContainerTests

- (void)setUp {
    [super setUp];
    self.obj = [SomeNestedObservableObject new];
    self.obj.child = [SomeObservableObject new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    XCTestExpectation *expectation = [[XCTestExpectation alloc] initWithDescription:@"ParentNumberChange"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), ^{
        Observer *observer;
        observer = [self.obj observe:@selector(numberInParent) options:0 byBlock: ^(id obj, NSDictionary *dict){
            [expectation fulfill];
        }];
        self.obj.numberInParent = 5;
    });
    XCTAssertEqual([XCTWaiter waitForExpectations:@[expectation] timeout:2], XCTWaiterResultCompleted);
}

@end
