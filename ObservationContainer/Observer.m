//
//  Observer.m
//  ObservationContainer
//
//  Created by fadi on 6/2/18.
//  Copyright © 2018 fadi. All rights reserved.
//

#import "Observer.h"

@implementation Observer {
    void *ctx;
}

@synthesize observedObject = _observedObject;
@synthesize keyPath = _keyPath;
@synthesize options = _options;
@synthesize notification = _notification;

- (instancetype)init
{
    self = [super init];
    if (self) {
        [NSException raise:NSInternalInconsistencyException format:@"This class hasn't an empty initializer"];
    }
    return self;
}

- (instancetype)initWithObservedObject:(id) observedObject andKeyPath:(NSString *)keyPath
                            andOptions:(NSKeyValueObservingOptions) options
               andNotificationFunction:(void(^)(id obj, NSDictionary<NSKeyValueChangeKey,id> *change)) notificationFunction
{
    self = [super init];
    if (self) {
        ctx = &ctx;
        _keyPath = keyPath;
        _observedObject = observedObject;
        _options = options;
        _notification = notificationFunction;
        [observedObject addObserver:self forKeyPath:keyPath options:options context:ctx];
    }
    return self;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if (context == ctx) {
        if (self.notification) self.notification(object, change);
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)dealloc
{
    [[self observedObject] removeObserver:self forKeyPath:[self keyPath]];
}

@end

@implementation NSObject (Observation)

-(Observer *) observe:(SEL)propertyGetterToObserve options:(NSKeyValueObservingOptions) options byBlock:(void(^)(id obj, NSDictionary<NSKeyValueChangeKey,id> *change))notification {
    return [[Observer alloc] initWithObservedObject:self andKeyPath:NSStringFromSelector(propertyGetterToObserve) andOptions:options andNotificationFunction:notification];
}

@end
