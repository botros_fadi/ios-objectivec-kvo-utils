//
//  Observer.h
//  ObservationContainer
//
//  Created by fadi on 6/2/18.
//  Copyright © 2018 fadi. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 An object that observes on a single keyPath.
 Initialization of this object adds the observation and deallocation removes the observation.
 The object is immutable, all its properties are readonly.
 */
@interface Observer : NSObject

@property (  copy, nonatomic, readonly) void(^notification)(id obj, NSDictionary<NSKeyValueChangeKey,id> *change);
@property (  copy, nonatomic, readonly) NSString *keyPath;
@property (  weak, nonatomic, readonly) id observedObject;
@property (assign, nonatomic, readonly) NSKeyValueObservingOptions options;

/**
 The initializer of the object, it has a side-effect, which is really adding the observer.

 @param observedObject The object to observe.
 @param keyPath The key path to observe its value change.
 @param options Observation options, see: NSKeyValueObservingOptions.
 @param notificationFunction The function to call when observation invoked.
 @return The observer object, deallocate this object to remove the observation.
 */
- (instancetype)initWithObservedObject:(id) observedObject andKeyPath:(NSString *)keyPath
                            andOptions:(NSKeyValueObservingOptions) options
               andNotificationFunction:(void(^)(id obj, NSDictionary<NSKeyValueChangeKey,id> *change)) notificationFunction;

@end


@interface NSObject (Observation)
/**
 A convenience function to be called on any NSObject, this creates the Observer object that represents
 a Key-Value Observation, this creation also adds the native observation.

 @param propertyGetterToObserve The selector which represents the getter of the property, this is only
   for safety instead of using the KeyPath as a string, this is INVALID in properties which has
   a property name different than getter's name,  like BOOL properties for example.
 @param options Key-Value Observation options, see: NSKeyValueObservingOptions.
 @param notification Notification function to be called when observer is notified.
 @return The observer object, deallocate this object to remove the observation.
 */
-(Observer *) observe:(SEL)propertyGetterToObserve options:(NSKeyValueObservingOptions) options byBlock:(void(^)(id obj, NSDictionary<NSKeyValueChangeKey,id> *change))notification;
@end
