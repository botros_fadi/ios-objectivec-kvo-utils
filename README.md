# ios-objectivec-kvo-utils

### What is it?

Key-Value Observation is a great tool in Objective-C/Swift, but it has an unreadable and bad syntax, also it is error
prone, where you could forget to remove the observation and exception happens, also the observation function is very
difficult to write and read, and will lead to a nested `if` conditions.

### Motivation

Swift 4 has an `NSKeyValueObservation` object, this makes it very easy, just give it a function then you have it. Why
not make a similar construct in Objective-C ??

### Usage

```objc
        Observer *observer;
        self.obj = [SomeObject new];
        observer = [self.obj observe:@selector(propertyInSomeObject) options:0 byBlock: ^(id obj, NSDictionary *dict){
            NSLog(@"The property is changed");
        }];
        self.obj.propertyInSomeObject = 7;
        // When the object "observer" is deallocated, the observation is deinstalled (removed) automatically.
```

### License

Apache License V2.0